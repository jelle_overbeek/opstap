// Declare application, including of modules (see them as plugins) made by others.
var app = angular.module('app', ['ui.router', 'ngAnimate', 'angularUtils.directives.dirPagination', 'chart.js']);

// Main configuration of the application
app.config(function ($stateProvider, $urlRouterProvider, $locationProvider) {
    
    // States
    $stateProvider
        //[domain]/dashbaord will load dashboard.html into the main view element
        .state("dashboard", {
            url: "/dashboard",
            views: {
                "main": {
                    templateUrl: "/templates/dashboard/dashboard.html"
                }
            }
        })
        //[domain]/ongeclaimde-reizen will load unclaimed-events.html into the main view element
        .state("unclaimedEvents", {
            url: "/ongeclaimde-reizen",
            views: {
                "main": {
                    templateUrl: "/templates/unclaimed-events/unclaimed-events.html",
                    controller: "unclaimedEventsController"
                }
            }
        })
        // [domain]/mijn-reizen will load claimed-events.html into the main view element
        .state("claimedEvents", {
            url: "/mijn-reizen",
            views: {
                "main": {
                    templateUrl: "/templates/claimed-events/claimed-events.html",
                    controller: "claimedEventsController"
                }
            }
        })
        // [domain]/chauffeurs will load drivers.html into the main view element
        .state("drivers", {
            url: "/chauffeurs",
            views: {
                "main": {
                    templateUrl: "/templates/drivers/drivers.html",
                    controller: "driverController"
                }
            }
        });

    // If the link on the page doesn't exist, redirect to the dashboard.
    $urlRouterProvider.otherwise('/dashboard');
});
// Dashboard Services
// Services are functions that return the JSON returned by the call. In this case stubs are used because there is no back-end yet.

app.service("incomeService", ['$http', function($http) {
    return {
        getIncome: function() {
            return $http.get('/assets/stubs/income.json');
        }
    };
}]).service("costumerSatisfactionService", ['$http', function($http) {
    return {
        getScore: function() {
            return $http.get('/assets/stubs/costumer-satisfaction.json');
        }
    };
}]);
// Driver services
// Services are functions that return the JSON returned by the call. In this case stubs are used because there is no back-end yet.

app.service("driversService", ['$http', function($http) {
    return {
        getDrivers: function() {
            return $http.get('/assets/stubs/drivers.json');
        }
    };
}]);
// Event services
// Services are functions that return the JSON returned by the call. In this case stubs are used because there is no back-end yet.

app.service("eventsService", ['$http', function($http) {
    return {
        getUnclaimedEvents: function() {
            return $http.get('/assets/stubs/unclaimed-events.json');
        },
        getMyEvents: function() {
            return $http.get('/assets/stubs/claimed-events.json');
        }
    };
}]);
// Global services
// Services are functions that return the JSON returned by the call. In this case stubs are used because there is no back-end yet.

app.service("accountService", ['$http', function($http) {
    return {
        getAccount: function() {
            return $http.get('/assets/stubs/account.json');
        }
    };
}]);
// Global filters
// All filters that can be used in the app.

app.filter('distance', function() {
    return function(value) {
        return Math.round(value) + ' km';
    };
});
// Dashboard directives
// Here will dashboard templates be linked to their controllers

app.directive('incomeWidget', function () {
        return {
            restrict: 'E',
            templateUrl: '/templates/dashboard/income-widget.html',
            controller: 'incomeController'
        }
    })
    .directive('accountWidget', function () {
        return {
            restrict: 'E',
            templateUrl: '/templates/dashboard/account-widget.html',
            controller: 'accountController'
        }
    })
    .directive('costumerSatisfactionWidget', function () {
        return {
            restrict: 'E',
            templateUrl: '/templates/dashboard/costumer-satisfaction-widget.html',
            controller: 'costumerSatisfactionController'
        }
    })
    .directive('feedbackWidget', function () {
        return {
            restrict: 'E',
            templateUrl: '/templates/dashboard/feedback-widget.html'
        }
    });
// Directives used for events
// Here will event templates be linked to their controllers

app.directive('eventModal', function() {
    return {
        restrict: 'E',
        templateUrl: '/templates/modals/event-modal.html',
        controller: "eventModalController",
        scope: { event: '=' }
    }
});
// Global directives
// Here will global templates be linked to their controllers

app.directive('header', function() {
    return {
        restrict: 'E',
        templateUrl: '/templates/header/header.html'
    }
});
// Dashboard controller
app.controller('incomeController', ['$scope', 'incomeService', '$filter', function ($scope, incomeService, $filter) {

        // Defining variables
        $scope.totalIncome = 0;
        $scope.totalDistance = 0;
        $scope.sortOrder = 'up';

        // run incomeService
        incomeService.getIncome().then(function (result) {

            // Making data available in template
            $scope.events = result.data.data;

        }).finally(function () {

            // Calculating total income and total distance
            angular.forEach($scope.events, function (value, key) {
                $scope.totalIncome += value.price;
                $scope.totalDistance += value.distance;
            });

        });

        // From https://docs.angularjs.org/api/ng/filter/orderBy
        var orderBy = $filter('orderBy');

        $scope.order = function (predicate) {
            $scope.predicate = predicate;
            $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
            $scope.events = orderBy($scope.events, predicate, $scope.reverse);

            if ($scope.reverse === true) {
                $scope.sortOrder = 'down';
            } else {
                $scope.sortOrder = 'up';
            }

        };

        // Ordering is set on date by default
        $scope.order('date', true);

}]).controller('costumerSatisfactionController', ['$scope', 'costumerSatisfactionService', function ($scope, costumerSatisfactionService) {

    // Chart.js variables to style the graphs on the dashboard
    $scope.chartOptions = {
        segmentShowStroke : false,
        percentageInnerCutout : 75, // This is 0 for Pie charts
        animationSteps : 100,
        animationEasing : "easeOutQuart",
        animateScale : false,
        showTooltips: false,
        maintainAspectRatio: false
    };

    // Colors used in the chart
    $scope.chartColors = [ '#5CB579', '#F1F1F1'];

    // Get score from JSON file
    costumerSatisfactionService.getScore().then(function (result) {

        // Make data available in the template
        $scope.data = result.data.data;
    });

}]);
// Driver controllers

app.controller('driverController', ['$scope', 'driversService', '$filter', function ($scope, driversService, $filter) {

    // variables
    $scope.loading = true;

    // get the drivers from JSON file
    driversService.getDrivers().then(function (result) {
        $scope.drivers = result.data.data;

        console.log($scope.drivers);

    }).finally(function () {

        // set loading to false because the service is finished getting data
        $scope.loading = false;

    });

    // From https://docs.angularjs.org/api/ng/filter/orderBy
    var orderBy = $filter('orderBy');

    $scope.order = function (predicate) {
        $scope.predicate = predicate;
        $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
        $scope.events = orderBy($scope.events, predicate, $scope.reverse);

        if ($scope.reverse === true) {
            $scope.sortOrder = 'down';
        } else {
            $scope.sortOrder = 'up';
        }

    };

    $scope.order('last_name', true);

    // Add new driver to array
    $scope.addNewDriver = function () {
        $scope.drivers.push({
            id: $scope.drivers.length + 1,
            isActive: true,
            avatar: "http://placehold.it/25x25",
            avatar_2x: "http://placehold.it/50/50",
            name: $scope.first_name + $scope.last_name,
            first_name: $scope.first_name,
            last_name: $scope.last_name,
            email: $scope.email
        });

        $scope.first_name = "";
        $scope.last_name = "";
        $scope.email = "";

        $scope.new_driver.first_name.$setPristine();
        $scope.new_driver.last_name.$setPristine();
        $scope.new_driver.email.$setPristine();
    };

    // Remove driver from array
    $scope.removeDriver = function (driverId) {
        $scope.drivers.pop(driverId);
    }



}]);
// unclaimed controllers

app.controller('unclaimedEventsController', ['$scope', 'eventsService', '$filter', function($scope, eventsService, $filter) {

    // variables
    $scope.selectedEvent = false;
    $scope.loading = true;

    // get the unclaimed events from JSON file
    eventsService.getUnclaimedEvents().then(function (result) {

        // make data available in the template
        $scope.events = result.data.data;

    }).finally(function () {

        // set loading to false because the service is finished getting data
        $scope.loading = false;

    });

    $scope.toggleEvent = function(event) {
        $scope.selectedEvent = event
    };

    // From https://docs.angularjs.org/api/ng/filter/orderBy
    var orderBy = $filter('orderBy');

    $scope.order = function (predicate) {
        $scope.predicate = predicate;
        $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
        $scope.events = orderBy($scope.events, predicate, $scope.reverse);

        if ($scope.reverse === true) {
            $scope.sortOrder = 'down';
        } else {
            $scope.sortOrder = 'up';
        }

    };

    // Default ordering is set to passengers
    $scope.order('passengers', true);

    // Check if any events have passengers below a quantity of 10
    $scope.checkPassengers = function(events) {
        for (var i = 0;  i < events.length; i++) {
            if(events[i].passengers < 10 && events[i].passengers > 0) {
                return true
            }
        }
    };
    
}]);


app.controller('claimedEventsController', ['$scope', 'eventsService', '$filter', function($scope, eventsService, $filter) {

    // variables
    $scope.selectedEvent = false;
    $scope.loading = true;
    $scope.myEventsLoading = true;
    $scope.myHistoryLoading = true;

    // get the events associated with the account from JSON file
    eventsService.getMyEvents().then(function (result) {

        // make data available in template
        $scope.events = result.data.data;

    }).finally(function () {

        // Done loading
        $scope.myEventsLoading = false;
    });

    // Function to check if end_time of the event is already passed, this function is not in use.
    $scope.checkDate = function (event) {
        var now = Date.now() / 1000;

        if(now >= event.end_time) {
            return true
        }
    };

    // Set selected event to the clicked event
    $scope.toggleEvent = function(event) {
        $scope.selectedEvent = event
    };

    // From https://docs.angularjs.org/api/ng/filter/orderBy
    var orderBy = $filter('orderBy');

    $scope.order = function (predicate) {
        $scope.predicate = predicate;
        $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
        $scope.events = orderBy($scope.events, predicate, $scope.reverse);

        if ($scope.reverse === true) {
            $scope.sortOrder = 'down';
        } else {
            $scope.sortOrder = 'up';
        }

    };

    // Set default ordering to start_time
    $scope.order('start_time', true);

    // Check if any events have passengers that are below 10
    $scope.checkPassengers = function(events) {
        for (var i = 0;  i < events.length; i++) {
            if(events[i].passengers < 10 && events[i].passengers > 0) {
                return true
            }
        }
    };

}]);

app.controller('eventModalController', ['$scope', 'driversService', function($scope, driversService) {

    // Get drivers for the dropdown
    driversService.getDrivers().then(function (result) {
        $scope.drivers = result.data.data;
    });

    // Make empty object that will be filled with claim information
    $scope.claimObj = {
        selectedPassengers: 0,
        selectedPassengersTemp: 0
    };

    // Reset choosed clusters
    function resetClaimed() {
        for (var i = 0;  i < $scope.event.clusters.length; i++) {
            $scope.event.clusters[i].claimed = false;
        }
    }

    // Function to make a claim
    $scope.claim = function(el) {

        // if a cluster is clicked but it is already been claimed, do nothing
        if(!el.claimSubmitted) {

            // Reset selected passengers
            $scope.claimObj.selectedPassengers = 0;

            resetClaimed();

            // Set all clusters before the selected cluster as claimed
            for (var i = 0;  i <= el.index; i++) {

                // Set claimed for clusters
                $scope.event.clusters[i].claimed = true;

                // count up all the passengers before and from the selected cluster
                $scope.claimObj.selectedPassengers +=  $scope.event.clusters[i].quantity;
            }
        }
    };


    // This function will show the user the result of clicking by giving a preview on mouse over
    $scope.claimEnter = function(el) {

        if(!el.claimSubmitted) {

            // Reset temporary selected passengers
            $scope.claimObj.selectedPassengersTemp = 0;

            // Set all clusters before the selected cluster as claiming
            for (var i = 0;  i <= el.index; i++) {

                // Set claiming for clusters
                $scope.event.clusters[i].claiming = true;

                // count up all the passengers before and from the selected cluster
                $scope.claimObj.selectedPassengersTemp +=  $scope.event.clusters[i].quantity;
            }
        }
    };

    // Function to clear temporary selection when user stops hovering
    $scope.claimLeave = function(el) {
        for (var i = 0;  i <= el.index; i++) {
            $scope.event.clusters[i].claiming = false;
        }

        // Set all clusters before the selected cluster as claiming
        $scope.claimObj.selectedPassengersTemp = 0;
    };

    // Fucntion to close modal
    $scope.close = function() {

        resetClaimed();

        // Reset claim object
        $scope.claimObj = {
            selectedPassengers: 0,
            selectedPassengersTemp: 0
        };

        // Reset validation
        $scope.claimForm.driver.$setPristine();

        // Clear selected event
        $scope.event = undefined;
    };

    // This function should submit the claim object to the server when there is a back-end. For now the object will log on the console.
    $scope.submitClaim = function() {

        console.log($scope.claimObj);
        resetClaimed();

        $scope.claimObj = {
            selectedPassengers: 0,
            selectedPassengersTemp: 0
        };

        $scope.claimForm.driver.$setPristine();
        $scope.event = undefined;
    };

    // Google static map variables
    var mapWidth = 270,
        mapHeight = 200,
        mapSize = mapWidth + "x" + mapHeight,
        mapSize_2x = mapWidth*2 + "x" + mapHeight*2,
        mapKey = "AIzaSyCNuPoKpH9VKxNvgJaLpdInG2Bn2mTdmKQ",
        departureColor = "ff6f6f",
        destinationColor = "21BBE5";

    // If the event changes to another event, refresh variables and objects
    $scope.$watch('event', function() {
        if($scope.event) {

            if($scope.event.claimedPassengers) {
                $scope.claimObj = {
                    selectedPassengers: $scope.event.claimedPassengers,
                    selectedPassengersTemp: 0
                }
            } else {
                $scope.claimObj = {
                    selectedPassengers: 0,
                    selectedPassengersTemp: 0
                }
            }

            $scope.claimObj.eventId = $scope.event.id;
            $scope.staticMap    = "http://maps.googleapis.com/maps/api/staticmap?autoscale=1&size=" + mapSize + "&maptype=roadmap&markers=size:mid%7Ccolor:0x" + destinationColor + "%7Clabel:%7C" + $scope.event.destination_lat + "," + $scope.event.destination_long + "&markers=size:mid%7Ccolor:0x" + departureColor + "%7Clabel:%7C" + $scope.event.departure_lat + "," + $scope.event.departure_long + "&key=" + mapKey;
            $scope.staticMap2x  = "http://maps.googleapis.com/maps/api/staticmap?autoscale=2&size=" + mapSize_2x + "&maptype=roadmap&markers=size:mid%7Ccolor:0x" + destinationColor + "%7Clabel:%7C" + $scope.event.destination_lat + "," + $scope.event.destination_long + "&markers=size:mid%7Ccolor:0x" + departureColor + "%7Clabel:%7C" + $scope.event.departure_lat + "," + $scope.event.departure_long + "&key=" + mapKey;
        }
    });
    
}]);
// Global controllers

app.controller('accountController', ['$scope', 'accountService', function ($scope, accountService) {

    $scope.account = undefined;

    // run accountService
    accountService.getAccount().then(function (result) {
        $scope.account = result.data.data;
    }).finally(function () {

    });

}]);