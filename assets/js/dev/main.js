// Declare application, including of modules (see them as plugins) made by others.
var app = angular.module('app', ['ui.router', 'ngAnimate', 'angularUtils.directives.dirPagination', 'chart.js']);

// Main configuration of the application
app.config(function ($stateProvider, $urlRouterProvider, $locationProvider) {
    
    // States
    $stateProvider
        //[domain]/dashbaord will load dashboard.html into the main view element
        .state("dashboard", {
            url: "/dashboard",
            views: {
                "main": {
                    templateUrl: "/templates/dashboard/dashboard.html"
                }
            }
        })
        //[domain]/ongeclaimde-reizen will load unclaimed-events.html into the main view element
        .state("unclaimedEvents", {
            url: "/ongeclaimde-reizen",
            views: {
                "main": {
                    templateUrl: "/templates/unclaimed-events/unclaimed-events.html",
                    controller: "unclaimedEventsController"
                }
            }
        })
        // [domain]/mijn-reizen will load claimed-events.html into the main view element
        .state("claimedEvents", {
            url: "/mijn-reizen",
            views: {
                "main": {
                    templateUrl: "/templates/claimed-events/claimed-events.html",
                    controller: "claimedEventsController"
                }
            }
        })
        // [domain]/chauffeurs will load drivers.html into the main view element
        .state("drivers", {
            url: "/chauffeurs",
            views: {
                "main": {
                    templateUrl: "/templates/drivers/drivers.html",
                    controller: "driverController"
                }
            }
        });

    // If the link on the page doesn't exist, redirect to the dashboard.
    $urlRouterProvider.otherwise('/dashboard');
});