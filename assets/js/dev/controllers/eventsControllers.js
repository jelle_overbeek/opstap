// unclaimed controllers

app.controller('unclaimedEventsController', ['$scope', 'eventsService', '$filter', function($scope, eventsService, $filter) {

    // variables
    $scope.selectedEvent = false;
    $scope.loading = true;

    // get the unclaimed events from JSON file
    eventsService.getUnclaimedEvents().then(function (result) {

        // make data available in the template
        $scope.events = result.data.data;

    }).finally(function () {

        // set loading to false because the service is finished getting data
        $scope.loading = false;

    });

    $scope.toggleEvent = function(event) {
        $scope.selectedEvent = event
    };

    // From https://docs.angularjs.org/api/ng/filter/orderBy
    var orderBy = $filter('orderBy');

    $scope.order = function (predicate) {
        $scope.predicate = predicate;
        $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
        $scope.events = orderBy($scope.events, predicate, $scope.reverse);

        if ($scope.reverse === true) {
            $scope.sortOrder = 'down';
        } else {
            $scope.sortOrder = 'up';
        }

    };

    // Default ordering is set to passengers
    $scope.order('passengers', true);

    // Check if any events have passengers below a quantity of 10
    $scope.checkPassengers = function(events) {
        for (var i = 0;  i < events.length; i++) {
            if(events[i].passengers < 10 && events[i].passengers > 0) {
                return true
            }
        }
    };
    
}]);


app.controller('claimedEventsController', ['$scope', 'eventsService', '$filter', function($scope, eventsService, $filter) {

    // variables
    $scope.selectedEvent = false;
    $scope.loading = true;
    $scope.myEventsLoading = true;
    $scope.myHistoryLoading = true;

    // get the events associated with the account from JSON file
    eventsService.getMyEvents().then(function (result) {

        // make data available in template
        $scope.events = result.data.data;

    }).finally(function () {

        // Done loading
        $scope.myEventsLoading = false;
    });

    // Function to check if end_time of the event is already passed, this function is not in use.
    $scope.checkDate = function (event) {
        var now = Date.now() / 1000;

        if(now >= event.end_time) {
            return true
        }
    };

    // Set selected event to the clicked event
    $scope.toggleEvent = function(event) {
        $scope.selectedEvent = event
    };

    // From https://docs.angularjs.org/api/ng/filter/orderBy
    var orderBy = $filter('orderBy');

    $scope.order = function (predicate) {
        $scope.predicate = predicate;
        $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
        $scope.events = orderBy($scope.events, predicate, $scope.reverse);

        if ($scope.reverse === true) {
            $scope.sortOrder = 'down';
        } else {
            $scope.sortOrder = 'up';
        }

    };

    // Set default ordering to start_time
    $scope.order('start_time', true);

    // Check if any events have passengers that are below 10
    $scope.checkPassengers = function(events) {
        for (var i = 0;  i < events.length; i++) {
            if(events[i].passengers < 10 && events[i].passengers > 0) {
                return true
            }
        }
    };

}]);

app.controller('eventModalController', ['$scope', 'driversService', function($scope, driversService) {

    // Get drivers for the dropdown
    driversService.getDrivers().then(function (result) {
        $scope.drivers = result.data.data;
    });

    // Make empty object that will be filled with claim information
    $scope.claimObj = {
        selectedPassengers: 0,
        selectedPassengersTemp: 0
    };

    // Reset choosed clusters
    function resetClaimed() {
        for (var i = 0;  i < $scope.event.clusters.length; i++) {
            $scope.event.clusters[i].claimed = false;
        }
    }

    // Function to make a claim
    $scope.claim = function(el) {

        // if a cluster is clicked but it is already been claimed, do nothing
        if(!el.claimSubmitted) {

            // Reset selected passengers
            $scope.claimObj.selectedPassengers = 0;

            resetClaimed();

            // Set all clusters before the selected cluster as claimed
            for (var i = 0;  i <= el.index; i++) {

                // Set claimed for clusters
                $scope.event.clusters[i].claimed = true;

                // count up all the passengers before and from the selected cluster
                $scope.claimObj.selectedPassengers +=  $scope.event.clusters[i].quantity;
            }
        }
    };


    // This function will show the user the result of clicking by giving a preview on mouse over
    $scope.claimEnter = function(el) {

        if(!el.claimSubmitted) {

            // Reset temporary selected passengers
            $scope.claimObj.selectedPassengersTemp = 0;

            // Set all clusters before the selected cluster as claiming
            for (var i = 0;  i <= el.index; i++) {

                // Set claiming for clusters
                $scope.event.clusters[i].claiming = true;

                // count up all the passengers before and from the selected cluster
                $scope.claimObj.selectedPassengersTemp +=  $scope.event.clusters[i].quantity;
            }
        }
    };

    // Function to clear temporary selection when user stops hovering
    $scope.claimLeave = function(el) {
        for (var i = 0;  i <= el.index; i++) {
            $scope.event.clusters[i].claiming = false;
        }

        // Set all clusters before the selected cluster as claiming
        $scope.claimObj.selectedPassengersTemp = 0;
    };

    // Fucntion to close modal
    $scope.close = function() {

        resetClaimed();

        // Reset claim object
        $scope.claimObj = {
            selectedPassengers: 0,
            selectedPassengersTemp: 0
        };

        // Reset validation
        $scope.claimForm.driver.$setPristine();

        // Clear selected event
        $scope.event = undefined;
    };

    // This function should submit the claim object to the server when there is a back-end. For now the object will log on the console.
    $scope.submitClaim = function() {

        console.log($scope.claimObj);
        resetClaimed();

        $scope.claimObj = {
            selectedPassengers: 0,
            selectedPassengersTemp: 0
        };

        $scope.claimForm.driver.$setPristine();
        $scope.event = undefined;
    };

    // Google static map variables
    var mapWidth = 270,
        mapHeight = 200,
        mapSize = mapWidth + "x" + mapHeight,
        mapSize_2x = mapWidth*2 + "x" + mapHeight*2,
        mapKey = "AIzaSyCNuPoKpH9VKxNvgJaLpdInG2Bn2mTdmKQ",
        departureColor = "ff6f6f",
        destinationColor = "21BBE5";

    // If the event changes to another event, refresh variables and objects
    $scope.$watch('event', function() {
        if($scope.event) {

            if($scope.event.claimedPassengers) {
                $scope.claimObj = {
                    selectedPassengers: $scope.event.claimedPassengers,
                    selectedPassengersTemp: 0
                }
            } else {
                $scope.claimObj = {
                    selectedPassengers: 0,
                    selectedPassengersTemp: 0
                }
            }

            $scope.claimObj.eventId = $scope.event.id;
            $scope.staticMap    = "http://maps.googleapis.com/maps/api/staticmap?autoscale=1&size=" + mapSize + "&maptype=roadmap&markers=size:mid%7Ccolor:0x" + destinationColor + "%7Clabel:%7C" + $scope.event.destination_lat + "," + $scope.event.destination_long + "&markers=size:mid%7Ccolor:0x" + departureColor + "%7Clabel:%7C" + $scope.event.departure_lat + "," + $scope.event.departure_long + "&key=" + mapKey;
            $scope.staticMap2x  = "http://maps.googleapis.com/maps/api/staticmap?autoscale=2&size=" + mapSize_2x + "&maptype=roadmap&markers=size:mid%7Ccolor:0x" + destinationColor + "%7Clabel:%7C" + $scope.event.destination_lat + "," + $scope.event.destination_long + "&markers=size:mid%7Ccolor:0x" + departureColor + "%7Clabel:%7C" + $scope.event.departure_lat + "," + $scope.event.departure_long + "&key=" + mapKey;
        }
    });
    
}]);