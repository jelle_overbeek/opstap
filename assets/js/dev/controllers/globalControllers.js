// Global controllers

app.controller('accountController', ['$scope', 'accountService', function ($scope, accountService) {

    $scope.account = undefined;

    // run accountService
    accountService.getAccount().then(function (result) {
        $scope.account = result.data.data;
    }).finally(function () {

    });

}]);