// Dashboard controller
app.controller('incomeController', ['$scope', 'incomeService', '$filter', function ($scope, incomeService, $filter) {

        // Defining variables
        $scope.totalIncome = 0;
        $scope.totalDistance = 0;
        $scope.sortOrder = 'up';

        // run incomeService
        incomeService.getIncome().then(function (result) {

            // Making data available in template
            $scope.events = result.data.data;

        }).finally(function () {

            // Calculating total income and total distance
            angular.forEach($scope.events, function (value, key) {
                $scope.totalIncome += value.price;
                $scope.totalDistance += value.distance;
            });

        });

        // From https://docs.angularjs.org/api/ng/filter/orderBy
        var orderBy = $filter('orderBy');

        $scope.order = function (predicate) {
            $scope.predicate = predicate;
            $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
            $scope.events = orderBy($scope.events, predicate, $scope.reverse);

            if ($scope.reverse === true) {
                $scope.sortOrder = 'down';
            } else {
                $scope.sortOrder = 'up';
            }

        };

        // Ordering is set on date by default
        $scope.order('date', true);

}]).controller('costumerSatisfactionController', ['$scope', 'costumerSatisfactionService', function ($scope, costumerSatisfactionService) {

    // Chart.js variables to style the graphs on the dashboard
    $scope.chartOptions = {
        segmentShowStroke : false,
        percentageInnerCutout : 75, // This is 0 for Pie charts
        animationSteps : 100,
        animationEasing : "easeOutQuart",
        animateScale : false,
        showTooltips: false,
        maintainAspectRatio: false
    };

    // Colors used in the chart
    $scope.chartColors = [ '#5CB579', '#F1F1F1'];

    // Get score from JSON file
    costumerSatisfactionService.getScore().then(function (result) {

        // Make data available in the template
        $scope.data = result.data.data;
    });

}]);