// Driver controllers

app.controller('driverController', ['$scope', 'driversService', '$filter', function ($scope, driversService, $filter) {

    // variables
    $scope.loading = true;

    // get the drivers from JSON file
    driversService.getDrivers().then(function (result) {
        $scope.drivers = result.data.data;

        console.log($scope.drivers);

    }).finally(function () {

        // set loading to false because the service is finished getting data
        $scope.loading = false;

    });

    // From https://docs.angularjs.org/api/ng/filter/orderBy
    var orderBy = $filter('orderBy');

    $scope.order = function (predicate) {
        $scope.predicate = predicate;
        $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
        $scope.events = orderBy($scope.events, predicate, $scope.reverse);

        if ($scope.reverse === true) {
            $scope.sortOrder = 'down';
        } else {
            $scope.sortOrder = 'up';
        }

    };

    $scope.order('last_name', true);

    // Add new driver to array
    $scope.addNewDriver = function () {
        $scope.drivers.push({
            id: $scope.drivers.length + 1,
            isActive: true,
            avatar: "http://placehold.it/25x25",
            avatar_2x: "http://placehold.it/50/50",
            name: $scope.first_name + $scope.last_name,
            first_name: $scope.first_name,
            last_name: $scope.last_name,
            email: $scope.email
        });

        $scope.first_name = "";
        $scope.last_name = "";
        $scope.email = "";

        $scope.new_driver.first_name.$setPristine();
        $scope.new_driver.last_name.$setPristine();
        $scope.new_driver.email.$setPristine();
    };

    // Remove driver from array
    $scope.removeDriver = function (driverId) {
        $scope.drivers.pop(driverId);
    }



}]);