// Global filters
// All filters that can be used in the app.

app.filter('distance', function() {
    return function(value) {
        return Math.round(value) + ' km';
    };
});