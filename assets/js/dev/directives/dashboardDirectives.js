// Dashboard directives
// Here will dashboard templates be linked to their controllers

app.directive('incomeWidget', function () {
        return {
            restrict: 'E',
            templateUrl: '/templates/dashboard/income-widget.html',
            controller: 'incomeController'
        }
    })
    .directive('accountWidget', function () {
        return {
            restrict: 'E',
            templateUrl: '/templates/dashboard/account-widget.html',
            controller: 'accountController'
        }
    })
    .directive('costumerSatisfactionWidget', function () {
        return {
            restrict: 'E',
            templateUrl: '/templates/dashboard/costumer-satisfaction-widget.html',
            controller: 'costumerSatisfactionController'
        }
    })
    .directive('feedbackWidget', function () {
        return {
            restrict: 'E',
            templateUrl: '/templates/dashboard/feedback-widget.html'
        }
    });