// Directives used for events
// Here will event templates be linked to their controllers

app.directive('eventModal', function() {
    return {
        restrict: 'E',
        templateUrl: '/templates/modals/event-modal.html',
        controller: "eventModalController",
        scope: { event: '=' }
    }
});