// Global directives
// Here will global templates be linked to their controllers

app.directive('header', function() {
    return {
        restrict: 'E',
        templateUrl: '/templates/header/header.html'
    }
});