// Event services
// Services are functions that return the JSON returned by the call. In this case stubs are used because there is no back-end yet.

app.service("eventsService", ['$http', function($http) {
    return {
        getUnclaimedEvents: function() {
            return $http.get('/assets/stubs/unclaimed-events.json');
        },
        getMyEvents: function() {
            return $http.get('/assets/stubs/claimed-events.json');
        }
    };
}]);