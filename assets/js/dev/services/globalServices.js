// Global services
// Services are functions that return the JSON returned by the call. In this case stubs are used because there is no back-end yet.

app.service("accountService", ['$http', function($http) {
    return {
        getAccount: function() {
            return $http.get('/assets/stubs/account.json');
        }
    };
}]);