// Dashboard Services
// Services are functions that return the JSON returned by the call. In this case stubs are used because there is no back-end yet.

app.service("incomeService", ['$http', function($http) {
    return {
        getIncome: function() {
            return $http.get('/assets/stubs/income.json');
        }
    };
}]).service("costumerSatisfactionService", ['$http', function($http) {
    return {
        getScore: function() {
            return $http.get('/assets/stubs/costumer-satisfaction.json');
        }
    };
}]);