// Driver services
// Services are functions that return the JSON returned by the call. In this case stubs are used because there is no back-end yet.

app.service("driversService", ['$http', function($http) {
    return {
        getDrivers: function() {
            return $http.get('/assets/stubs/drivers.json');
        }
    };
}]);