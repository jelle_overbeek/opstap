/**
 * Created by jelleoverbeek on 19-05-16.
 */

'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');

gulp.task('sass', function () {
    gulp.src('./assets/scss/style.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./assets/css/'));
});

gulp.task('watch:sass', function () {
    gulp.watch('./assets/**/*.scss', ['sass']);
});

gulp.task('js', function () {
    gulp.src([
        './assets/js/dev/main.js',
        './assets/js/dev/services/*.js',
        './assets/js/dev/filters/*.js',
        './assets/js/dev/directives/*.js',
        './assets/js/dev/controllers/*.js'

    ])
        .pipe(concat('app.js'))
        .pipe(gulp.dest('./assets/js/'));
});

gulp.task('watch:js', function () {
    gulp.watch('./assets/js/dev/**/*.js', ['js']);
});

gulp.task('default', ['sass', 'js']);